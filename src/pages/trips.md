---
layout: page
title: "Trips"
permalink: "/trips/"
---

# Future

<ul>
  {% for trip in site.trips %}
    {% if trip.state == 'future' %}
        <li>
            {{ trip.type }} <a href="{{ trip.url }}">{{ trip.title }}</a> - {{ trip.headline }}
        </li>
    {% endif %}
  {% endfor %}
</ul>

# Past

<ul>
  {% for trip in site.trips %}
    {% if trip.state == 'past' %}
        <li>
            {{ trip.type }} <a href="{{ trip.url }}">{{ trip.title }}</a> - {{ trip.headline }}
        </li>
    {% endif %}
  {% endfor %}
</ul>