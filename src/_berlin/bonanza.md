---
layout: page
title: "Bonanza Coffee Heroes"
headline: "Coffee shop"
type: ☕️
tags:
  coffee
  sweets
---

Best coffee place in Berlin.

Oderberger Str. 35

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2426.6382069440706!2d13.403157216187937!3d52.539979779818076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47a851f9034567c9%3A0x693e223689da353c!2sBonanza+Coffee+Heroes!5e0!3m2!1sen!2sde!4v1542059694800" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>