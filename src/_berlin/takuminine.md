---
layout: page
title: "Takumi Nine"
headline: "Ramen"
type: 🍜
tags:
  ramen
  lunch
  dinner
  japaneses
---

Tiny place that serves ramen. Veggie friendly options.

Pappelallee 19, 10437 Berlin

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2426.426536599051!2d13.413504716123915!3d52.54380907981862!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47a8520044ed38c5%3A0x8576b01265e363e1!2sTakumi+NINE!5e0!3m2!1sen!2sde!4v1542484039458" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
