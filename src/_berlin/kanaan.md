---
layout: page
title: "Kanaan"
headline: "Middle Eastern"
type: 🥙
tags:
  hummus
  falafel
  Shakshuka
---

Kanaan never dissapoints. Best falafel and hummus in Berlin.
Try the Shakshuka (if available), thank me later :)

Kopenhagener Str. 17, 10437 Berlin

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2426.1295286872055!2d13.40539011612411!3d52.54918187981973!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47a852048dec7eab%3A0x56c8075a22c8bcfa!2sKanaan+Berlin!5e0!3m2!1sen!2sde!4v1542484376533" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
