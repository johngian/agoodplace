---
layout: page
title: "Soy"
headline: "Vietnamese cuisine"
type: 🍲
tags:
  vietnamese
  vegan
---

Vegan vietnamese restaurant located in Mitte. Plenty of tofu/seitan options.
Try the curry and the vietnamese pancakes. Dish sharing friendly.

Rosa-Luxemburg-Straße 30

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2427.408548372179!2d13.409382515807705!3d52.52604197981535!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47a84e1e0ac97e6d%3A0x50a08ce6431ab541!2sSOY!5e0!3m2!1sen!2sde!4v1542484777564" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
