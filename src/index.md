---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

{% for city in site.collections %}
  <h2>
    <a href="{{ site.baseurl }}/{{ city.label }}">
      {{ city.pretty_name }}
    </a>
  </h2>
  <ul>
    {% for entry in city.docs %}
      <li>
        <a href="{{ entry.url }}">
          {{ entry.type }} {{ entry.title }}
        </a>
      </li>
    {% endfor %}
  </ul>
{% endfor %}