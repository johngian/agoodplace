---
layout: page
title: "Iceland"
headline: "Iceland Airwaves 2018"
state: "past"
type: 🇮🇸
---

Nov 7 - 12th 2018

For [Iceland Airwaves Music festival](http://icelandairwaves.is/) 20th edition.

![empty stage showing iceland airwaves 20 years banner](/assets/img/IMG_5231.jpg)

<iframe allow="autoplay *; encrypted-media *;" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/de/playlist/iceland-airwaves-2018/pl.u-yNJmTq6yD6d?l=en"></iframe>

[Playlist of the artists I got to see performing live](https://itunes.apple.com/de/playlist/iceland-airwaves-2018/pl.u-yNJmTq6yD6d?l=en). Some highlights include
* Kaelan Mikla (IS)
* Mammút (IS)
* Between Mountains (IS)
* Aurora
* Snail Mail
* Soccer Mommy
* Stella Donnelly

Enjoyed [KEXP's home base at the festival](https://kexp.org/read/2018/11/6/kexp-live-iceland-airwaves-2018-day-1-president-bongo-ottar-saemundsen/) at the KEX Hostel and their special shows which included Aurora and Team Dreams. As well as [The Current at Skúli Craft Bar](https://www.thecurrent.org/collection/iceland-airwaves) where we got to see Stella Donnelly and Soccer Mommy for a more intimate acoustic shows.

![KEXP stage at KEX Hostel](/assets/img/IMG_5226.jpg)

![The Current stage at Skúli Bar](/assets/img/IMG_5203.jpg)

![kexp "on air" lights](/assets/img/IMG_5215.jpg)
